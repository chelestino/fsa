$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();

    $('.edit-user').click(function () {

        var userId = $(this).data('id');

        var url = '/users/edit/'+userId;
        $.ajax({
            url: url,
            success: function(html) {
                $('.edit_profile').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('.edit_profile')
                );
            }
        });
    })

});
          