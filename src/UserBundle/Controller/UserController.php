<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
 use UserBundle\Entity\User;

/**
 * @Route("/users")
 */
class UserController extends Controller
{

    /**
     * @Route("/", name="users_list")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('UserBundle:User')->findAll();

        return $this->render('UserBundle:Users:index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/registration", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $regForm = $this->createForm('UserBundle\Form\RegistrationType');

        $regForm->handleRequest($request);

        if ($regForm->isSubmitted() && $regForm->isValid()) {

            $user = $regForm->getData();

            $user->setEnabled(true);

            $userManager->updateUser($user, true);

            return $this->redirectToRoute('users_list');
        }

        return $this->render('UserBundle:Forms:register.html.twig', array(
            'reg_form' => $regForm->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="edit_user")
     * @param $id
     */
    public function editAction(Request $request, User $user)
    {
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $em = $this->getDoctrine()->getManager();

        $profileForm = $this->createForm('UserBundle\Form\ProfileFormType', $user, [
            'action' =>$this->generateUrl('edit_user', ['id' => $user->getId()]),
        ]);

        $profileForm->handleRequest($request);

        if ($profileForm->isSubmitted() && $profileForm->isValid()) {

            $user = $profileForm->getData();

            $userManager->updateUser($user, true);

            return $this->redirectToRoute('users_list');
        }

        return $this->render('UserBundle:Forms:profile.html.twig', array(
            'profile_form' => $profileForm->createView(),
        ));
    }
}